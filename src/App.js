import logo from './logo.svg';
import './App.css';
import Ex_Shoes_Redux from './Ex_Shoes_Redux/Ex_Shoes_Redux';

function App() {
  return (
    <div className="App">
    <Ex_Shoes_Redux/>
    </div>
  );
}

export default App;
