import React, { Component } from "react";
import { connect } from "react-redux";
import { data_shoes } from "./data_shoes";
import GioHang from "./GioHang";
import ItemShoe from "./ItemShoe";
import { ADD_TO_CART } from "./redux/constanst/shoeConstants";

class Ex_Shoes_Redux extends Component {
  state = {
    shoes: data_shoes,
    gioHang: [],
  };

  renderContent = () => {
    return this.props.listShoe.map((item) => {
      return (
        <ItemShoe
          handleAddToCart={this.props.handleAddToCartRedux}
          data={item}
        />
      );
    });
  };
  render() {
    return (
      <div className="container py-5">
        <GioHang />
        <div className="row">{this.renderContent()}</div>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    listShoe: state.shoeReducer.shoes,
    cart: state.shoeReducer.gioHang,
  };
};
let mapDispatchToProps = (dispatch) => {
  return {
    handleAddToCartRedux: (shoe) => {
      dispatch({
        type: ADD_TO_CART,
        payload: shoe,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Ex_Shoes_Redux);
