import { data_shoes } from "../../data_shoes";
import { ADD_TO_CART, CHANGE_QUANTITY } from "../constanst/shoeConstants";

let initialState = {
  shoes: data_shoes,
  gioHang: [],
};
export let shoeReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ADD_TO_CART: {
      let index = state.gioHang.findIndex((item) => {
        return item.id == payload.id;
      });
      let cloneGioHang = [...state.gioHang];

      if (index == -1) {
        let newSp = { ...payload, quantity: 1 };
        cloneGioHang.push(newSp);
      } else {
        cloneGioHang[index].quantity++;
      }
      state.gioHang = cloneGioHang;
      return { ...state };
    }
    case CHANGE_QUANTITY: {
      console.log("yes");
      let cloneGioHang = [...state.gioHang];
      let index = state.gioHang.findIndex((shoe) => {
        return (shoe.id == payload.idShoe);
      });
      if (index != -1) {
        cloneGioHang[index].quantity +=  payload.quantity
        cloneGioHang[index].quantity <= 0 && cloneGioHang.splice(index, 1);
      }
      state.gioHang = cloneGioHang;
      return { ...state };
    }
    default:
      return state;
  }
};
